## Moodle OpenCV Laboratory
by Christian Bermejo, Christine Saavedra

Requirement under Sir Alampay (CS129.18)

### To Do
See included pdf

### Requirements
1. Python 2.7
2. Numpy (1.10.x and up)
3. OpenCV 3

### Useful Links
1. Installation and Setup in Windows

  - http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_setup/py_setup_in_windows/py_setup_in_windows.html

2. Determining Canny Edge Threshold
  - http://www.pyimagesearch.com/2015/04/06/zero-parameter-automatic-canny-edge-detection-with-python-and-opencv/  
