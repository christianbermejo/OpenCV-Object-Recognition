# import the necessary packages
from __future__ import print_function
from sklearn.naive_bayes import MultinomialNB
import numpy as np
import pandas as pd
import argparse
import csv
import cv2
import glob
import os
import random

'''
In OpenCV Python,
Mat objects are represented as numpy arrays
Point objects may be represented by tuples
Check documentation for more information on the choices
'''
# set to print everything output by numpy on console
np.set_printoptions(threshold='nan')

# set up tuple to be used as Point for findContours
point = (0,0)
#random number generator for drawing contours

# source: http://www.pyimagesearch.com/2015/04/06/zero-parameter-automatic-canny-edge-detection-with-python-and-opencv/
# compute median of the
def auto_canny(image, sigma=0.50):
    v = np.median(image)
    #apply automatic Canny edge detection using computed median
    lower = int(max(0,(1.0 - sigma) * v))
    upper = int(min(255,(1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
    return edged

def buildSobel(image):
    # this should take a grayscale image
    gradX = cv2.Sobel(image, cv2.CV_16S, 1, 0)
    absGradX = cv2.convertScaleAbs(gradX)

    gradY = cv2.Sobel(image, cv2.CV_16S, 0, 1)
    absGradY = cv2.convertScaleAbs(gradY)

    sobelMat = cv2.addWeighted(absGradX, 0.5, absGradY, 0.5, 0)

    return sobelMat

def ROI(name, image, sobel, edges, dataset):
    # defining contours and hierarchy
    # https://fossies.org/diffs/opencv/2.4.11_vs_3.0.0-rc1/samples/python2/contours.py-diff.html
    _, contours, hierarchy = cv2.findContours(edges.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # original dataset of contour coordinates
    dataset = np.empty((0,4))
    i = 0
    for cnt in contours:
        # find coordinates for region of interest
        x,y,w,h = cv2.boundingRect(cnt)
        if (w*h > 800):
            # get roi from image and sobel
            roiPic = image[y:y+h, x:x+w]
            roiSobelPic = sobel[y:y+h, x:x+w]
            # cv2.imwrite('./roidataset/' + name[:-4] + str(i) + '.png', roiPic)
            cv2.imwrite('./roidataset/' + name + 'sobel' + str(i) + '.png', roiSobelPic)
            # create an array from these coordinates
            roi = np.array(cv2.boundingRect(cnt)).reshape(1,4)
            dataset = np.append(dataset, roi, 0)
        i += 1
    return dataset

def readCSV(csvFile, image):
    testSet = pd.read_csv(str(csvFile) + '.csv', header=None, na_values=['.'])
    predictions = testSet.iloc[:,65]
    for i in range(len(predictions)):
        x,y,w,h = dataset[i]
        if predictions[i] == 0:
            cv2.rectangle(image,(int(x),int(y)),(int(x+w),int(y+h)),(200,0,0),1)
        if predictions[i] == 1:
            cv2.rectangle(image,(int(x),int(y)),(int(x+w),int(y+h)),(0,0,200),1)

def writeTestSetCSV():
    with open('testset.csv', 'wb') as fp:
        # clears csv file
        fp.truncate()
        a = csv.writer(fp, delimiter=',')
        for file in glob.glob('./roidataset/*.png'): # to add: filepath
            pos = np.zeros([1,0])
            newimg = cv2.resize(cv2.imread(file), (8,8))
            for i in range(newimg.shape[0]):
                for j in range(newimg.shape[1]):
                    # hack: imwrite in Python saves to 3 channels by default
                    pos = np.append(pos, int(newimg[i,j][0]))
            a.writerow(pos)

def mNaiveBayes():
    df = pd.read_csv('trainingset.csv', header=None, na_values=['.'])
    test = pd.read_csv('testset.csv', header=None, na_values=['.'])
    targets = df.iloc[:,64]
    features = df.columns[0:63]
    nb = MultinomialNB()
    mnb = nb.fit(df[features], targets).predict(test[features])
    test.loc[:,64] = pd.Series(mnb, index=test.index)
    test.to_csv('testset.csv', header=None, na_values=['.'])

def cleanUp():
    for file in glob.glob('./roidataset/*.png'):
        os.remove(file)


parser = argparse.ArgumentParser(description='Pattern Recognition Lab 2: Bermejo, Saavedra with OpenCV')
parser.add_argument('-i', '--image', required=True,
                   help='Open an image and execute script')
args = vars(parser.parse_args())

name= 'result'
# load the image
image = cv2.imread(args['image'])
# convert to grayscale as findContours only supports an 8-bit single channel image
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# perform canny edge detection
edges = auto_canny(gray)
# create sobel image of version
dst = buildSobel(gray)
# get coordinates for boundingRect
dataset = np.empty((0,4))
dataset = ROI(name, image, dst, edges, dataset)

writeTestSetCSV()
mNaiveBayes()

readCSV('testset', image)

cleanUp()

cv2.imwrite(name+'-FINAL.png', image)
# cv2.imshow("Original + Contours", image)
# cv2.imshow("Sobel", dst)
cv2.waitKey(10)
